import { Component, OnInit } from '@angular/core';
import { templateJitUrl } from '@angular/compiler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AngularCrud';
  time  : any

ngOnInit(){
  let date = new Date ();
  this.time=  `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
  document.getElementById("clock").innerHTML = this.time;
  setInterval(this.ngOnInit, 1000)
 // console.log(this.time)

}

  
}

