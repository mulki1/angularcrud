export class Employee{
    id: number;
    name: string;
    gender: string;
    email?: string;
    password?: string;
    confirmPassword?: string;
    phoneNumber?: number;
    contactPreference: string;
    dateOfBirth: Date;
    department: string;
    isActive: boolean;
    photoPath?: string;
}