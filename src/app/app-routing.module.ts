import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListEmployeesComponent } from './employees/list-employees.component';
import { CreateEmployeeComponent } from './employees/create-employee.component';
import { CreateEmployeeCanDeactiveGuardService } from './employees/create-employee-can-deactivate-guard.service';
import { EmployeeDetailsComponent } from './employees/employee-details.component';

const routes: Routes = [
  {path:'list' ,  component : ListEmployeesComponent},
  {path: 'create' ,  component : CreateEmployeeComponent,},// canDeactivate:[CreateEmployeeCanDeactiveGuardService]},
  {path: 'employee/:id' ,  component : EmployeeDetailsComponent,},
  {path:'' , redirectTo: '/list', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
