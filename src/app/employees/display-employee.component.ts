import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter, } from '@angular/core';
import { Employee } from '../models/employee.models';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-display-employee',
  templateUrl: './display-employee.component.html',
  styleUrls: ['./display-employee.component.css']
})
export class DisplayEmployeeComponent implements OnInit, OnChanges {
  private _employee
  @Input() employeeId: number;
  @Output() notify:  EventEmitter<Employee> = new EventEmitter<Employee>();
  private selectedEmployeeId: number;

  @Input() 
    
  set employee(val: Employee){
    // console.log('Previous :  ' + (this._employee ? this._employee.name : 'NULL'));
    // console.log('Current :' + val.name)
    this._employee = val;
  }

  get employee():Employee{
    return this._employee;
  }
  constructor( private _route: ActivatedRoute) { }

  ngOnInit() {
    this.selectedEmployeeId = +this._route.snapshot.paramMap.get('id');
  }

  ngOnChanges(changes : SimpleChanges){
    for (const propName of Object.keys(changes)){
      //console.log(propName)
      const change = changes[propName];
      const from = JSON.stringify(change.previousValue);
      const to =JSON.stringify(change.currentValue);

     // console.log(propName + 'change from  ' + from + 'to ' + to);
    }

  }

  handleClick(){
    this.notify.emit(this.employee);
  }

  // ngOnChanges(changes: SimpleChanges) {//called when an input property of the component changes
  //   const previousEmployee = <Employee>changes.employee.previousValue;
  //   const currentEmployee = <Employee>changes.employee.currentValue;

  //   console.log('previous :' + (previousEmployee ? previousEmployee.name : 'Null'));
  //   console.log('current :' + currentEmployee.name);

  //   //console.log (changes)
  // }

}
