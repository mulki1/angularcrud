import { Injectable } from "@angular/core";
import { Employee } from "../models/employee.models";

@Injectable()
export class EmployeeService{
    private listEmployees: Employee[]= [
        {
          id:1,
          name: 'Abdul',
          gender: 'male',
          email: 'mulki@g.com',
          phoneNumber: 908989,
          contactPreference:'email',
          dateOfBirth: new Date('10/25/1988'),
          department: 'IT',
          isActive: true,
          photoPath:'assets/images/abd.jpg' 
        },
      
        {
          id:2,
          name: 'zainab',
          gender: 'female',
          phoneNumber: 908989,
          contactPreference:'phoneNumber',
          dateOfBirth: new Date('10/25/1990'),
          department: 'HR',
          isActive: true,
          photoPath:'assets/images/zee.jpg' 
        },
      
        {
          id:3,
          name: 'Saha',
          gender: 'female',
          email: 'saha@g.com',
          contactPreference:'email',
          dateOfBirth: new Date('10/25/1992'),
          department: 'IT',
          isActive: false,
          photoPath:'assets/images/zs.jpg' 
        },
      
      ];

      getEmployees(): Employee[]{
          return this.listEmployees;
      }

      getEmployee(id: number): Employee{
        return this.listEmployees.find(e => e.id === id);
    }

      save(employee : Employee){
          this.listEmployees.push(employee);
      }

}