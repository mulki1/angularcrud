import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Department } from '../models/department';
import {BsDatepickerConfig} from 'ngx-bootstrap/datepicker';
import {Employee} from '../models/employee.models'
import {Router} from '@angular/router';
import {EmployeeService} from './employee.service'

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {
  @ViewChild('employeeForm') public createEmployeeForm : NgForm; 
  datePickerConfig: Partial <BsDatepickerConfig>;
  DateOfBirth : Date = new Date (2018, 11, 1);
  previewPhoto = false;
employee: Employee={
  id: null,
  name: null,
  gender: null,
  email: null,
  password: null,
  confirmPassword:null,
  phoneNumber: null,
  contactPreference: null,
  dateOfBirth: null,
  department: "1",
  isActive:null,
  photoPath: null,
};


  gender = 'male';
  departmentList: Department[] = [{ value: 1, name: 'Help Desk' }, { value: 2, name: 'HR' }, { value: 3, name: 'IT' }, { value: 4, name: 'Payroll' },]

  constructor(private _router: Router, private _employeeService: EmployeeService) {
    this.datePickerConfig=Object.assign({},
       {
         containerClass: 'theme-dark-blue',
        showWeekNumbers: false,
        minDate: new Date(2018, 0, 1),
        maxDate : new Date(2018, 11, 31), 
        dateInputFormat: 'DD/MM/YYYY'
        })
   }

  ngOnInit() {
  }

  saveEmployee(): void {
    //console.log(newEmployee);
    this._employeeService.save(this.employee);
    this._router.navigate(['list'])

  }

  togglePhotoPreview(){
    this.previewPhoto=!this.previewPhoto
  }

  alert(){
    alert("welcome on board");
    return false
  }


}
 